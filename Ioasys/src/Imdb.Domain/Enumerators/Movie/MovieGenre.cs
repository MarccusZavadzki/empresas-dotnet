﻿namespace Imdb.Domain.Enumerators.Movie {

    public enum MovieGenre {

        COMEDY = 1,
        ACTION = 2,
        DRAMA = 3,
        HORROR = 4,
        ROMANCE = 5,
        ANIMACAO = 6,
        UNDEFINED = 99

    }
}
