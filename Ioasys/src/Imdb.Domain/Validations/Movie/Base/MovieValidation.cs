﻿using FluentValidation;
using Imdb.Domain.Commands.Movie.Base;

namespace Imdb.Domain.Validations.Movie.Base {

    public abstract class MovieValidation<T> : AbstractValidator<T> where T : MovieCommand {

        #region Validation Messages
        public static string FieldRequired = "Invalid fields try again";
        #endregion

        protected void ValidateId() {
            RuleFor(c => c.Id).NotNull().WithMessage(FieldRequired);
        }

        protected void ValidateName() {
            RuleFor(c => c.Name).NotNull().WithMessage(FieldRequired);
        }

        protected void ValidateDirector() {
            RuleFor(c => c.MovieDirector).NotNull().WithMessage(FieldRequired);
        }

        protected void ValidateGenre() {
            RuleFor(c => c.Genre).NotNull().WithMessage(FieldRequired);
        }

        protected void ValidateDuration() {
            RuleFor(c => c.Duration).NotNull().WithMessage(FieldRequired);
        }

        protected void ValidateSynopsis() {
            RuleFor(c => c.Synopsis).NotNull().WithMessage(FieldRequired);
        }

    }
}
