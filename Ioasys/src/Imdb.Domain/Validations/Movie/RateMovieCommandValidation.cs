﻿using Imdb.Domain.Commands.Movie;
using Imdb.Domain.Validations.Movie.Base;

namespace Imdb.Domain.Validations.Movie {
    public class RateMovieCommandValidation : MovieValidation<RateMovieCommand> {

        public RateMovieCommandValidation() {
            ValidateId();
        }

    }
}
