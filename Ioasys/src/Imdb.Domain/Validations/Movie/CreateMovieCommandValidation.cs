﻿using Imdb.Domain.Commands.Movie;
using Imdb.Domain.Validations.Movie.Base;
using System;
using System.Collections.Generic;
using System.Text;

namespace Imdb.Domain.Validations.Movie {

    public class CreateMovieCommandValidation : MovieValidation<CreateMovieCommand> {

        public CreateMovieCommandValidation() {
            ValidateName();
            ValidateDirector();
            ValidateGenre();
            ValidateDuration();
            ValidateSynopsis();
        }
    
    }

}
