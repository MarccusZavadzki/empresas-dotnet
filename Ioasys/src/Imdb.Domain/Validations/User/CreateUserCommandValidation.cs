﻿using Imdb.Domain.Commands.User;
using Imdb.Domain.Validations.User.Base;

namespace Imdb.Domain.Validations.User {

    public class CreateUserCommandValidation : UserValidation<CreateUserCommand> {

        public CreateUserCommandValidation() {
            ValidateName();
            ValidateBirthDate();
            ValidateIsAdm();
            ValidateEmail();
            ValidatePassword();
        }

    }
}
