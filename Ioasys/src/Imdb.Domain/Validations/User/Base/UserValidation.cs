﻿using FluentValidation;
using Imdb.Domain.Commands.User.Base;

namespace Imdb.Domain.Validations.User.Base {

    public abstract class UserValidation<T> : AbstractValidator<T> where T : UserCommand {

        #region Validation Messages
        public static string FieldRequired = "Invalid fields try again";
        #endregion

        protected void ValidateId() {
            RuleFor(c => c.Id).NotNull().WithMessage(FieldRequired);
        }

        protected void ValidateName() {
            RuleFor(c => c.Name).NotNull().WithMessage(FieldRequired);
        }

        protected void ValidateBirthDate() {
            RuleFor(c => c.BirthDate).NotNull().WithMessage(FieldRequired);
        }

        protected void ValidateEmail() {
            RuleFor(c => c.Email).NotNull().WithMessage(FieldRequired);
        }

        protected void ValidatePassword() {
            RuleFor(c => c.Password).NotNull().WithMessage(FieldRequired);
        }

        protected void ValidateIsAdm() {
            RuleFor(c => c.IsAdm).NotNull().WithMessage(FieldRequired);
        }

    }

}
