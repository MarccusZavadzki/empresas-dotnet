﻿using Imdb.Domain.Commands.User;
using Imdb.Domain.Validations.User.Base;

namespace Imdb.Domain.Validations.User {

    public class UpdateUserCommandValidation : UserValidation<UpdateUserCommand> {

        public UpdateUserCommandValidation() {
            ValidateId();
        }

    }
}
