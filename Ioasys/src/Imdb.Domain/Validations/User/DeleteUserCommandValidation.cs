﻿using FluentValidation;
using Imdb.Domain.Commands.User;

namespace Imdb.Domain.Validations.User {

    public class DeleteUserCommandValidation : AbstractValidator<DeleteUserCommand> {

        public DeleteUserCommandValidation() {
            RuleFor(c => c.Id).NotNull().WithMessage("Invalid fields try again");
        }

    }
}
