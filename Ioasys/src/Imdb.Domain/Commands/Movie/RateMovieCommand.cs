﻿using Imdb.Domain.Commands.Movie.Base;
using Imdb.Domain.Validations.Movie;

namespace Imdb.Domain.Commands.Movie {

    public class RateMovieCommand : MovieCommand {

        public RateMovieCommand(int idUser, int idMovie, int note) {
            Id = idMovie;
            IdUser = idUser;
            Note = note;
        }

        public int IdUser { get; private set; }
        public int Note { get; private set; }

        public override bool IsValid() {
            ValidationResult = new RateMovieCommandValidation().Validate(this);
            return ValidationResult.IsValid;
        }
    }
}
