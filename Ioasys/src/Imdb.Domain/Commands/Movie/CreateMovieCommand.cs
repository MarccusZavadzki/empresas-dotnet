﻿using Imdb.Domain.Commands.Movie.Base;
using Imdb.Domain.Enumerators.Movie;
using Imdb.Domain.Validations.Movie;
using System;
using System.Collections.Generic;

namespace Imdb.Domain.Commands.Movie {

    public class CreateMovieCommand : MovieCommand {

        public CreateMovieCommand(string name, string director, string genre, decimal duration, string synopsis, List<string> actors) {
            Name = name;
            MovieDirector = director;
            Genre = Enum.TryParse(genre, out MovieGenre enumGenre) ? enumGenre : MovieGenre.UNDEFINED;
            Duration = duration;
            Synopsis = synopsis;
            Actors = actors;
        }

        public List<string> Actors { get; private set; }

        public override bool IsValid() {
            ValidationResult = new CreateMovieCommandValidation().Validate(this);
            return ValidationResult.IsValid;
        }
    }
}
