﻿using Imdb.Domain.Core.Models;
using Imdb.Domain.Enumerators.Movie;
using System;
using Model = Imdb.Domain.Models.Entities;

namespace Imdb.Domain.Commands.Movie.Base {

    public abstract class MovieCommand : Command<Model.Movie>{

        public int Id { get; set; }
        public DateTime CreationDate { get; set; }
        public DateTime UpdateDate { get; set; }
        public string Name { get; set; }
        public string MovieDirector { get; set; }
        public MovieGenre Genre { get; set; }
        public decimal Duration { get; set; }
        public string Synopsis { get; set; }

    }
}
