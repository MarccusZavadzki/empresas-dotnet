﻿using Imdb.Domain.Commands.User.Base;
using Imdb.Domain.Validations.User;
using System;

namespace Imdb.Domain.Commands.User {

    public class CreateUserCommand : UserCommand {

        public CreateUserCommand(string name, DateTime birthDate, bool isAdm, string email, string password) {
            Name = name;
            BirthDate = birthDate;
            IsAdm = isAdm;
            Email = email;
            Password = password;
        }

        public override bool IsValid() {
            ValidationResult = new CreateUserCommandValidation().Validate(this);
            return ValidationResult.IsValid;
        }
    }
}
