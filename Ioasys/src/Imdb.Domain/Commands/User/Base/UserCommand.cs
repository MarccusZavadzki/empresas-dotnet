﻿using Imdb.Domain.Core.Models;
using System;
using Model = Imdb.Domain.Models.Entities;

namespace Imdb.Domain.Commands.User.Base {

    public abstract class UserCommand : Command<Model.User> {

        public int Id { get; set; }
        public DateTime CreationDate { get; set; }
        public DateTime UpdateDate { get; set; }
        public string Name { get; set; }
        public DateTime? BirthDate { get; set; }
        public bool? IsAdm { get; set; } //Nullable por causa do update
        public bool? IsAtive { get; set; } //Nullable por causa do update
        public string Email { get; set; }
        public string Password { get; set; }

    }
}
