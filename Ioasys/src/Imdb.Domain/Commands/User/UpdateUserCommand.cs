﻿using Imdb.Domain.Commands.User.Base;
using Imdb.Domain.Validations.User;
using System;

namespace Imdb.Domain.Commands.User {

    public class UpdateUserCommand : UserCommand {

        public UpdateUserCommand(int id, string name, DateTime? birthDate, bool? isAdm, bool? isAtive, string email, string password) {
            Id = id;
            Name = name;
            BirthDate = birthDate;
            IsAdm = isAdm;
            IsAtive = isAtive;
            Email = email;
            Password = password;
        }

        public override bool IsValid() {
            ValidationResult = new UpdateUserCommandValidation().Validate(this);
            return ValidationResult.IsValid;
        }
    }
}
