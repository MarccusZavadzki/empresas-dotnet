﻿using Imdb.Domain.Core.Models;
using Imdb.Domain.Validations.User;

namespace Imdb.Domain.Commands.User {

    public class DeleteUserCommand : Command {

        public DeleteUserCommand(int id) {
            Id = id;
        }

        public int Id { get; }

        public override bool IsValid() {
            ValidationResult = new DeleteUserCommandValidation().Validate(this);
            return ValidationResult.IsValid;
        }
    }
}
