﻿using System;

namespace Imdb.Domain.Interfaces.Entities {

    public interface IBaseEntity {

        int Id { get; set; }
        DateTime CreationDate { get; set; }
        DateTime UpdateDate { get; set; }

    }

}
