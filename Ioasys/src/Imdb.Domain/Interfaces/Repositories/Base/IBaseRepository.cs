﻿using Imdb.Domain.Models.Entities.Base;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Imdb.Domain.Interfaces.Repositories.Base {

    public interface IBaseRepository<TEntity> : IDisposable where TEntity : BaseEntity {
        Task<List<TEntity>> GetAll();
        Task<TEntity> GetById(int id);
        Task<TEntity> Insert(TEntity item);
        Task<TEntity> Update(TEntity item);
        Task Delete(int id);
    }

}
