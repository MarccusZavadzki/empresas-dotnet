﻿using Imdb.Domain.Interfaces.Repositories.Base;
using Imdb.Domain.Models.Entities;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Imdb.Domain.Interfaces.Repositories {

    public interface IUserRepository : IBaseRepository<User> {

        Task<User> GetUserToLogin(string email, string password);
        Task<List<User>> GetUsersAtive();

    }
}
