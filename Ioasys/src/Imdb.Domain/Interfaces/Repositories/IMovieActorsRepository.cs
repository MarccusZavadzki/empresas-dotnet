﻿using Imdb.Domain.Interfaces.Repositories.Base;
using Imdb.Domain.Models.Entities;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Imdb.Domain.Interfaces.Repositories {
    public interface IMovieActorsRepository : IBaseRepository<MovieActors> {

        Task<List<string>> GetActorsByMovie(int movieId);

    }
}
