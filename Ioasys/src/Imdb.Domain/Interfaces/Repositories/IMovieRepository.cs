﻿using Imdb.Domain.Interfaces.Repositories.Base;
using Imdb.Domain.Models.Entities;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Imdb.Domain.Interfaces.Repositories {

    public interface IMovieRepository : IBaseRepository<Movie> {

        Task<List<Movie>> GetMoviesWithFilter(string name, string director, string genre);

    }
}
