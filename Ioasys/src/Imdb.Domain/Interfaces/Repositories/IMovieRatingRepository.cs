﻿using Imdb.Domain.Interfaces.Repositories.Base;
using Imdb.Domain.Models.Entities;
using System.Threading.Tasks;

namespace Imdb.Domain.Interfaces.Repositories {

    public interface IMovieRatingRepository : IBaseRepository<MovieRating> {
        Task<decimal?> GetAverageNoteByMovie(int movieId);
    }

}
