﻿using Imdb.Domain.Models.Entities.Base;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Imdb.Domain.Models.Entities {

    public class User : BaseEntity {

        public User() { }

        public User(string name, DateTime birthDate, bool isAdm, bool isAtive, string email, string password) {
            Name = name;
            BirthDate = birthDate;
            IsAdm = isAdm;
            IsAtive = isAtive;
            Email = email;
            Password = password;
        }

        [Required()]
        [Column("name")]
        [MaxLength(100)]
        public string Name { get; set; }

        [Required()]
        [Column("dta_birth")]
        public DateTime BirthDate { get; set; }

        [Required()]
        [Column("flg_adm")]
        public bool IsAdm { get; set; }

        [Required()]
        [Column("flg_ative")]
        public bool IsAtive { get; set; }

        [Required()]
        [Column("email")]
        [MaxLength(50)]
        public string Email { get; set; }

        [Required()]
        [Column("password")]
        [MaxLength(20)]
        public string Password { get; set; }

        /* EF Relations */
        public virtual List<MovieRating> Notes { get; set; }
    }
}
