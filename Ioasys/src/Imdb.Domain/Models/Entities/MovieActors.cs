﻿using Imdb.Domain.Models.Entities.Base;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Imdb.Domain.Models.Entities {

    public class MovieActors : BaseEntity {

        public MovieActors() { }

        public MovieActors(int idMovie, string nameActor) {
            IdMovie = idMovie;
            NameActor = nameActor;
        }

        [Required()]
        [Column("name_actor")]
        [MaxLength(100)]
        public string NameActor { get; set; }

        /* EF Relations */
        [ForeignKey("Movie"), DatabaseGenerated(DatabaseGeneratedOption.None)]
        public int IdMovie { get; set; }
        public Movie Movie { get; set; }
    }

}
