﻿using Imdb.Domain.Enumerators.Movie;
using Imdb.Domain.Models.Entities.Base;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Imdb.Domain.Models.Entities {

    public class Movie : BaseEntity {

        public Movie() { }

        public Movie(string name, string movieDirector, MovieGenre genre, decimal duration, string synopsis) {
            Name = name;
            MovieDirector = movieDirector;
            Genre = genre;
            Duration = duration;
            Synopsis = synopsis;
        }

        [Required()]
        [Column("name")]
        [MaxLength(100)]
        public string Name { get; set; }

        [Required()]
        [Column("movie_director")]
        [MaxLength(100)]
        public string MovieDirector { get; set; }

        [Required()]
        [Column("genre")]
        public MovieGenre Genre { get; set; }

        [Required()]
        [Column("duration")]
        public decimal Duration { get; set; }

        [Required()]
        [Column("synopsis")]
        public string Synopsis { get; set; }

        /* EF Relations */
        public virtual List<MovieActors> Actors { get; set; }
        public virtual List<MovieRating> Notes { get; set; }
    }

}
