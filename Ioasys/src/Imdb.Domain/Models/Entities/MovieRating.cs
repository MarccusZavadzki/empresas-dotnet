﻿using Imdb.Domain.Models.Entities.Base;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Imdb.Domain.Models.Entities {

    public class MovieRating : BaseEntity {

        public MovieRating() { }

        public MovieRating(int idMovie, int idUser, int note) {
            IdMovie = idMovie;
            IdUser = idUser;
            Note = note;
        }

        [Required()]
        [Column("note")]
        public int Note { get; set; }

        /* EF Relations */
        [ForeignKey("Movie"), DatabaseGenerated(DatabaseGeneratedOption.None)]
        public int IdMovie { get; set; }
        public Movie Movie { get; set; }

        [ForeignKey("User"), DatabaseGenerated(DatabaseGeneratedOption.None)]
        public int IdUser { get; set; }
        public User User { get; set; }

    }
}
