﻿using AutoMapper;
using Imdb.Domain.CommandHandlers.Base;
using Imdb.Domain.Commands.User;
using Imdb.Domain.Core.Bus;
using Imdb.Domain.Interfaces.Repositories;
using Imdb.Domain.Models.Entities;
using MediatR;
using System;
using System.Threading;
using System.Threading.Tasks;

namespace Imdb.Domain.CommandHandlers {

    public class UserCommandHandler : CommandHandler, IRequestHandler<CreateUserCommand, User>, IRequestHandler<UpdateUserCommand, User>, IRequestHandler<DeleteUserCommand, Unit> {

        private readonly IMapper _mapper;
        private readonly IUserRepository _userRepository;

        public UserCommandHandler(
            IMediatorHandler bus,
            IMapper mapper,
            IUserRepository userRepository
        ) : base(bus) {
            _mapper = mapper;
            _userRepository = userRepository;
        }

        public async Task<User> Handle(CreateUserCommand request, CancellationToken cancellationToken) {

            if(!request.IsValid()) {
                NotifyValidationErrors(request);
                return null;
            }

            var model = _mapper.Map<User>(request);
            model = await _userRepository.Insert(model);

            return model;
        }

        public async Task<User> Handle(UpdateUserCommand request, CancellationToken cancellationToken) {

            if(!request.IsValid()) {
                NotifyValidationErrors(request);
                return null;
            }

            var atualModel = await _userRepository.GetById(request.Id);

            if(!string.IsNullOrWhiteSpace(request.Name))
                atualModel.Name = request.Name;

            if(request.BirthDate != null)
                atualModel.BirthDate = request.BirthDate.Value;

            if(request.IsAdm != null)
                atualModel.IsAdm = request.IsAdm.Value;

            if(request.IsAtive != null)
                atualModel.IsAtive = request.IsAtive.Value;

            if(!string.IsNullOrWhiteSpace(request.Email))
                atualModel.Email = request.Email;

            if(!string.IsNullOrWhiteSpace(request.Password))
                atualModel.Email = request.Password;

            atualModel = await _userRepository.Update(atualModel);

            return atualModel;
        }

        public async Task<Unit> Handle(DeleteUserCommand request, CancellationToken cancellationToken) {

            if(!request.IsValid()) {
                NotifyValidationErrors(request);
                return Unit.Value;
            }

            var atualModel = await _userRepository.GetById(request.Id);
            atualModel.IsAtive = false;
            await _userRepository.Update(atualModel);
            
            return Unit.Value;
        }
    }
}
