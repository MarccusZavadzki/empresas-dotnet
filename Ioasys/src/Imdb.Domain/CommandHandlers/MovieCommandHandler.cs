﻿using AutoMapper;
using Imdb.Domain.CommandHandlers.Base;
using Imdb.Domain.Commands.Movie;
using Imdb.Domain.Core.Bus;
using Imdb.Domain.Core.Models;
using Imdb.Domain.Interfaces.Repositories;
using Imdb.Domain.Models.Entities;
using MediatR;
using System.Threading;
using System.Threading.Tasks;

namespace Imdb.Domain.CommandHandlers {

    public class MovieCommandHandler : CommandHandler, IRequestHandler<CreateMovieCommand, Movie>, IRequestHandler<RateMovieCommand, Movie> {
        
        private readonly IMediatorHandler _bus;
        private readonly IMapper _mapper;
        private readonly IMovieRepository _movieRepository;
        private readonly IMovieRatingRepository _movieRatingRepository;
        private readonly IMovieActorsRepository _movieActorsRepository;

        public MovieCommandHandler(
            IMediatorHandler bus,
            IMapper mapper,
            IMovieRepository movieRepository,
            IMovieRatingRepository movieRatingRepository,
            IMovieActorsRepository movieActorsRepository
        ) : base(bus) {
            _bus = bus;
            _mapper = mapper;
            _movieRepository = movieRepository;
            _movieRatingRepository = movieRatingRepository;
            _movieActorsRepository = movieActorsRepository;
        }

        public async Task<Movie> Handle(CreateMovieCommand request, CancellationToken cancellationToken) {

            if(!request.IsValid()) {
                NotifyValidationErrors(request);
                return null;
            }

            var movie = _mapper.Map<Movie>(request);

            var result = await _movieRepository.Insert(movie);

            foreach(var actor in request.Actors){

                MovieActors movieActor = new MovieActors(result.Id, actor);
                await _movieActorsRepository.Insert(movieActor);

            }

            return result;
        }

        public async Task<Movie> Handle(RateMovieCommand request, CancellationToken cancellationToken) {
            
            if(!request.IsValid()) {
                NotifyValidationErrors(request);
                return null;
            }

            var movie = await _movieRepository.GetById(request.Id);

            if(movie == null) {
                await _bus.RaiseEvent(new DomainNotification("MOVIE_RATE", $"O filme informado nao existe, id: {request.Id}"));
                return null;
            }

            var movieRate = new MovieRating(request.Id, request.IdUser, request.Note);

            await _movieRatingRepository.Insert(movieRate);

            return movie;
        }
    }
}
