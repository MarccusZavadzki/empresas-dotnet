﻿using AutoMapper;
using Imdb.ApplicationService.Interfaces;
using Imdb.ApplicationService.ViewModels.Request.Movie;
using Imdb.ApplicationService.ViewModels.Response;
using Imdb.Domain.Commands.Movie;
using Imdb.Domain.Core.Bus;
using Imdb.Domain.Interfaces.Repositories;
using Imdb.Domain.Models.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Imdb.ApplicationService.Services {

    public class MovieApplicationService : IMovieApplicationService {

        private readonly IMapper _mapper;
        private readonly IMovieRepository _movieRepository;
        private readonly IMovieActorsRepository _movieActorsRepository;
        private readonly IMovieRatingRepository _movieRatingRepository;
        private readonly IMediatorHandler _bus;

        public MovieApplicationService(
            IMapper mapper,
            IMediatorHandler bus,
            IMovieRepository movieRepository,
            IMovieActorsRepository movieActorsRepository,
            IMovieRatingRepository movieRatingRepository
        ) {
            _mapper = mapper;
            _bus = bus;
            _movieRepository = movieRepository;
            _movieActorsRepository = movieActorsRepository;
            _movieRatingRepository = movieRatingRepository;
        }

        public async Task<MovieResponse> Create(CreateMovieViewModel createMovieViewModel) {

            var command = _mapper.Map<CreateMovieCommand>(createMovieViewModel);
            var result = await _bus.SendCommand<CreateMovieCommand, Movie>(command);

            var response = _mapper.Map<MovieResponse>(result);
            response.Actors = createMovieViewModel.Actors;

            return response;
        }

        public async Task<MovieResponse> Get(int id) {

            var movie = await _movieRepository.GetById(id);

            if(movie == null)
                return null;

            var actors = await _movieActorsRepository.GetActorsByMovie(id);
            decimal? averageNote = await _movieRatingRepository.GetAverageNoteByMovie(id);

            var movieResponse = _mapper.Map<MovieResponse>(movie);
            movieResponse.Actors = actors;
            movieResponse.AverageNote = averageNote;

            return movieResponse;
        }

        public async Task<List<MovieResponse>> List(FilterMoviesViewModel filter, int page = 0) {

            var moviesFiltered = await _movieRepository.GetMoviesWithFilter(filter.Name, filter.MovieDirector, filter.Genre);

            List<Movie> newList = await FilterActorMovie(moviesFiltered, filter.Actors);

            List<MovieResponse> retorno = new List<MovieResponse>();

            foreach(var movie in newList) {

                var actors = await _movieActorsRepository.GetActorsByMovie(movie.Id);
                decimal? averageNote = await _movieRatingRepository.GetAverageNoteByMovie(movie.Id);

                var movieResponse = _mapper.Map<MovieResponse>(movie);
                movieResponse.Actors = actors;
                movieResponse.AverageNote = averageNote;

                retorno.Add(movieResponse);
            }

            retorno = retorno.OrderByDescending(x => x.AverageNote).ThenBy(x => x.Name).ToList();

            if(page != 0) {
                retorno = retorno.Skip((page-1) * 10).Take(10).ToList(); //10 per page
            }

            return retorno;
        }

        public async Task<MovieResponse> Vote(int idUser, int idMovie, int note) {

            var command = new RateMovieCommand(idUser, idMovie, note);

            var result = await _bus.SendCommand<RateMovieCommand, Movie>(command);

            if(result == null)
                return null;

            var actors = await _movieActorsRepository.GetActorsByMovie(idMovie);
            decimal? averageNote = await _movieRatingRepository.GetAverageNoteByMovie(idMovie);

            var response = _mapper.Map<MovieResponse>(result);

            response.Actors = actors;
            response.AverageNote = averageNote;

            return response;
        }

        private async Task<List<Movie>> FilterActorMovie(List<Movie> atualList, List<string> actors) {

            List<Movie> newFilter = atualList.ToList();

            if(actors != null && actors.Any()) {

                foreach(var movie in atualList) {

                    var actorsMovie = await _movieActorsRepository.GetActorsByMovie(movie.Id);

                    if(actorsMovie != null && actorsMovie.Any()) {

                        bool isRemove = false;

                        foreach(var actor in actors) {

                            if(!actorsMovie.Contains(actor)) {
                                isRemove = true;
                            }

                        }

                        if(isRemove) {
                            newFilter.Remove(movie);
                        }

                    } else {
                        newFilter.Remove(movie);
                    }

                }

            }

            return newFilter;

        }
    }
}
