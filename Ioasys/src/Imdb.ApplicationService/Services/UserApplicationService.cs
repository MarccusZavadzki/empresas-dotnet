﻿using AutoMapper;
using Imdb.ApplicationService.Interfaces;
using Imdb.ApplicationService.ViewModels.Request;
using Imdb.ApplicationService.ViewModels.Response;
using Imdb.Domain.Commands.User;
using Imdb.Domain.Core.Bus;
using Imdb.Domain.Interfaces.Repositories;
using Imdb.Domain.Models.Entities;
using MediatR;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Imdb.ApplicationService.Services {

    public class UserApplicationService : IUserApplicationService {

        private readonly IMapper _mapper;
        private readonly IUserRepository _userRepository;
        private readonly IMediatorHandler _bus;

        public UserApplicationService(
            IMapper mapper,
            IMediatorHandler bus,
            IUserRepository userRepository
        ) {
            _mapper = mapper;
            _bus = bus;
            _userRepository = userRepository;
        }

        public async Task<List<UserResponse>> GetAtiveUsers(int page) {

            var users = await _userRepository.GetUsersAtive();

            users = users.OrderBy(x => x.Name).ToList();

            if(page != 0) {
                users = users.Skip((page-1) * 10).Take(10).ToList(); //10 per page
            }

            var response = _mapper.Map<List<UserResponse>>(users);

            return response;
        }

        public async Task<UserResponse> Create(CreateUserViewModel createUserViewModel) {

            var command = _mapper.Map<CreateUserCommand>(createUserViewModel);
            var result = await _bus.SendCommand<CreateUserCommand, User>(command);

            var response = _mapper.Map<UserResponse>(result);
            return response;
        }

        public async Task<UserResponse> Update(UpdateUserViewModel updateUserViewModel) {

            var command = _mapper.Map<UpdateUserCommand>(updateUserViewModel);
            var result = await _bus.SendCommand<UpdateUserCommand, User>(command);

            var response = _mapper.Map<UserResponse>(result);
            return response;

        }

        public async Task Delete(int id) {

            var command = new DeleteUserCommand(id);
            await _bus.SendCommand(command);

        }

    }
}
