﻿using AutoMapper;
using Imdb.ApplicationService.ViewModels.Request;
using Imdb.ApplicationService.ViewModels.Request.Movie;
using Imdb.ApplicationService.ViewModels.Response;
using Imdb.Domain.Commands.Movie;
using Imdb.Domain.Commands.User;
using Imdb.Domain.Models.Entities;

namespace Imdb.ApplicationService.AutoMapper {

    public class AutomapperConfig : Profile {

        public AutomapperConfig() {

            #region MOVIE

            CreateMap<Movie, MovieResponse>()
                .ConstructUsing(x => new MovieResponse {
                    Duration = x.Duration,
                    Genre = x.Genre.ToString(),
                    Id = x.Id,
                    MovieDirector = x.MovieDirector,
                    Name = x.Name,
                    Synopsis = x.Synopsis
                })
                .ForAllOtherMembers(i => i.Ignore());

            CreateMap<CreateMovieCommand, Movie>()
                .ConstructUsing(x => new Movie(x.Name, x.MovieDirector, x.Genre, x.Duration, x.Synopsis))
                .ForAllOtherMembers(i => i.Ignore());

            CreateMap<CreateMovieViewModel, CreateMovieCommand>()
                .ConstructUsing(x => new CreateMovieCommand(x.Name, x.MovieDirector, x.Genre, x.Duration, x.Synopsis, x.Actors))
                .ForAllOtherMembers(i => i.Ignore());
            #endregion


            #region MOVIE

            CreateMap<User, UserResponse>()
                .ConstructUsing(x => new UserResponse {
                    Id = x.Id,
                    Name = x.Name,
                    IsAdm = x.IsAdm,
                    BirthDate = x.BirthDate,
                    Email = x.Email
                })
                .ForAllOtherMembers(i => i.Ignore());

            CreateMap<CreateUserCommand, User>()
                .ConstructUsing(x => new User(x.Name, x.BirthDate.Value, x.IsAdm.Value, true, x.Email, x.Password))
                .ForAllOtherMembers(i => i.Ignore());

            CreateMap<CreateUserViewModel, CreateUserCommand>()
                .ConstructUsing(x => new CreateUserCommand(x.Name, x.BirthDate, x.IsAdm, x.Email, x.Password))
                .ForAllOtherMembers(i => i.Ignore());
            CreateMap<UpdateUserViewModel, UpdateUserCommand>()
                .ConstructUsing(x => new UpdateUserCommand(x.Id, x.Name, x.BirthDate, x.IsAdm, x.IsAtive, x.Email, x.Password))
                .ForAllOtherMembers(i => i.Ignore());
            #endregion
        }

    }
}
