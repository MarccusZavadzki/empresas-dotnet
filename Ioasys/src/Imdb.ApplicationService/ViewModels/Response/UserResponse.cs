﻿using System;

namespace Imdb.ApplicationService.ViewModels.Response {

    public class UserResponse {

        public int Id { get; set; }
        public string Name { get; set; }
        public DateTime BirthDate { get; set; }
        public bool IsAdm { get; set; }
        public string Email { get; set; }

    }
}
