﻿using System.Collections.Generic;

namespace Imdb.ApplicationService.ViewModels.Response {

    public class MovieResponse {

        public int Id { get; set; }
        public string Name { get; set; }
        public string MovieDirector { get; set; }
        public string Genre { get; set; }
        public decimal Duration { get; set; }
        public string Synopsis { get; set; }
        public List<string> Actors { get; set; }
        public decimal? AverageNote { get; set; }

    }
}
