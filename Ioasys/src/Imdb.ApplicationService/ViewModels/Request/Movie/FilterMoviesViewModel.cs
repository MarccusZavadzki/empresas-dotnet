﻿using System.Collections.Generic;

namespace Imdb.ApplicationService.ViewModels.Request.Movie {

    public class FilterMoviesViewModel {

        public string Name { get; set; }
        public string MovieDirector { get; set; }
        public string Genre { get; set; }
        public List<string> Actors { get; set; }

    }
}
