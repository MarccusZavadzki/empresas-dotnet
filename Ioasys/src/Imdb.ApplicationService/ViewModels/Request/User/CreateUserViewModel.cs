﻿using System;

namespace Imdb.ApplicationService.ViewModels.Request {

    public class CreateUserViewModel {

        public string Name { get; set; }
        public DateTime BirthDate { get; set; }
        public bool IsAdm { get; set; }
        public string Email { get; set; }
        public string Password { get; set; }

    }
}
