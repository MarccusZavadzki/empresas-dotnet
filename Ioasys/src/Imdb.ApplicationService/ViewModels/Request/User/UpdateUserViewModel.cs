﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Imdb.ApplicationService.ViewModels.Request {

    public class UpdateUserViewModel {

        public int Id { get; set; }
        public string Name { get; set; }
        public DateTime? BirthDate { get; set; }
        public string Email { get; set; }
        public string Password { get; set; }
        public bool? IsAtive { get; set; }
        public bool? IsAdm { get; set; }

    }
}
