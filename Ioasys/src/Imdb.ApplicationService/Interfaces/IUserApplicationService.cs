﻿using Imdb.ApplicationService.ViewModels.Request;
using Imdb.ApplicationService.ViewModels.Response;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Imdb.ApplicationService.Interfaces {
    public interface IUserApplicationService {

        Task<List<UserResponse>> GetAtiveUsers(int page);

        Task<UserResponse> Create(CreateUserViewModel createUserViewModel);
        Task<UserResponse> Update(UpdateUserViewModel updateUserViewModel);
        Task Delete(int id);

    }
}
