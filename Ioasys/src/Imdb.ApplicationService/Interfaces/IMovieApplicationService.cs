﻿using Imdb.ApplicationService.ViewModels.Request.Movie;
using Imdb.ApplicationService.ViewModels.Response;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Imdb.ApplicationService.Interfaces {

    public interface IMovieApplicationService {

        Task<MovieResponse> Get(int id);
        Task<List<MovieResponse>> List(FilterMoviesViewModel filter, int page = 0);


        Task<MovieResponse> Create(CreateMovieViewModel createMovieViewModel);
        Task<MovieResponse> Vote(int idUser, int idMovie, int note);

    }
}
