﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace Imdb.Data.SqlServer.Migrations
{
    public partial class AddIdUserRating : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<int>(
                name: "IdUser",
                table: "MovieRating",
                type: "int",
                nullable: false,
                defaultValue: 0);

            migrationBuilder.CreateIndex(
                name: "IX_MovieRating_IdUser",
                table: "MovieRating",
                column: "IdUser");

            migrationBuilder.AddForeignKey(
                name: "FK_MovieRating_User_IdUser",
                table: "MovieRating",
                column: "IdUser",
                principalTable: "User",
                principalColumn: "id",
                onDelete: ReferentialAction.Restrict);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_MovieRating_User_IdUser",
                table: "MovieRating");

            migrationBuilder.DropIndex(
                name: "IX_MovieRating_IdUser",
                table: "MovieRating");

            migrationBuilder.DropColumn(
                name: "IdUser",
                table: "MovieRating");
        }
    }
}
