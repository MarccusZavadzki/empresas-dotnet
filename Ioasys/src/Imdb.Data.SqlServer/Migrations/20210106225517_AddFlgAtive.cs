﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace Imdb.Data.SqlServer.Migrations
{
    public partial class AddFlgAtive : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<bool>(
                name: "flg_ative",
                table: "User",
                type: "bit",
                nullable: false,
                defaultValue: false);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "flg_ative",
                table: "User");
        }
    }
}
