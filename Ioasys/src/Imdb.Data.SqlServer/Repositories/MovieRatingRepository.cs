﻿using Imdb.Data.SqlServer.Context;
using Imdb.Data.SqlServer.Repositories.Base;
using Imdb.Domain.Core.Bus;
using Imdb.Domain.Interfaces.Repositories;
using Imdb.Domain.Models.Entities;
using Microsoft.EntityFrameworkCore;
using System.Linq;
using System.Threading.Tasks;

namespace Imdb.Data.SqlServer.Repositories {

    public class MovieRatingRepository : BaseRepository<MovieRating>, IMovieRatingRepository {

        public MovieRatingRepository(ApplicationDbContext db, IMediatorHandler bus) : base(db, bus) {

        }

        public async Task<decimal?> GetAverageNoteByMovie(int movieId) {

            var movieRating = await DbSet.Where(x => x.IdMovie == movieId).ToListAsync();

            var listOfNotes = movieRating.Select(x => x.Note).ToList();

            if(listOfNotes.Count == 0) {
                return null;
            }

            int sum = listOfNotes.Sum();
            decimal average = sum / listOfNotes.Count;

            return average;
        }

    }

}
