﻿using Imdb.Data.SqlServer.Context;
using Imdb.Data.SqlServer.Repositories.Base;
using Imdb.Domain.Core.Bus;
using Imdb.Domain.Interfaces.Repositories;
using Imdb.Domain.Models.Entities;
using Microsoft.EntityFrameworkCore;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Imdb.Data.SqlServer.Repositories {

    public class UserRepository : BaseRepository<User>, IUserRepository {

        public UserRepository(ApplicationDbContext db, IMediatorHandler bus) : base(db, bus) {

        }

        public async Task<User> GetUserToLogin(string email, string password) {

            User user = await DbSet.Where(x => x.Email.Equals(email) && x.Password.Equals(password)).FirstOrDefaultAsync();

            return user;
        }

        public async Task<List<User>> GetUsersAtive() {

            List<User> users = await DbSet.Where(x => x.IsAtive == true && x.IsAdm == false).ToListAsync();

            return users;
        }

    }

}
