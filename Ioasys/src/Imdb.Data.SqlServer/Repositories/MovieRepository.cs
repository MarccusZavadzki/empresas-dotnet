﻿using Imdb.Data.SqlServer.Context;
using Imdb.Data.SqlServer.Repositories.Base;
using Imdb.Domain.Core.Bus;
using Imdb.Domain.Interfaces.Repositories;
using Imdb.Domain.Models.Entities;
using System.Collections.Generic;
using System.Threading.Tasks;
using System.Linq;
using Imdb.Domain.Enumerators.Movie;
using System;
using Microsoft.EntityFrameworkCore;

namespace Imdb.Data.SqlServer.Repositories {

    public class MovieRepository : BaseRepository<Movie>, IMovieRepository {

        public MovieRepository(ApplicationDbContext db, IMediatorHandler bus) : base(db, bus) {

        }

        public async Task<List<Movie>> GetMoviesWithFilter(string name, string director, string genre) {

            var qMovies = (from m in DbSet select m);

            if(!string.IsNullOrWhiteSpace(name))
                qMovies = qMovies.Where(x => x.Name.Equals(name));

            if(!string.IsNullOrWhiteSpace(director))
                qMovies = qMovies.Where(x => x.MovieDirector.Equals(director));

            if(!string.IsNullOrWhiteSpace(genre) && Enum.TryParse(genre, out MovieGenre enumGenre))
                qMovies = qMovies.Where(x => x.Genre.Equals(enumGenre));

            var ret = await qMovies.ToListAsync();
            return ret;
        }

    }

}
