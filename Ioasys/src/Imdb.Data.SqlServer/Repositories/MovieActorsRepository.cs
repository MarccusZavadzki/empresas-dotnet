﻿using Imdb.Data.SqlServer.Context;
using Imdb.Data.SqlServer.Repositories.Base;
using Imdb.Domain.Core.Bus;
using Imdb.Domain.Interfaces.Repositories;
using Imdb.Domain.Models.Entities;
using Microsoft.EntityFrameworkCore;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Imdb.Data.SqlServer.Repositories {

    public class MovieActorsRepository : BaseRepository<MovieActors>, IMovieActorsRepository {

        public MovieActorsRepository(ApplicationDbContext db, IMediatorHandler bus) : base(db, bus) {

        }

        public async Task<List<string>> GetActorsByMovie(int movieId) {

            var movieActors = await DbSet.Where(x => x.IdMovie == movieId).ToListAsync();

            return movieActors.Select(x => x.NameActor).ToList();
        }

    }

}
