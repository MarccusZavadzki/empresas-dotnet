﻿using Imdb.Domain.Core.Models;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace Imdb.Domain.Core.Bus {

    public interface IMediatorHandler {

        Task SendCommand<T>(T command) where T : Command;
        Task<TResponse> SendCommand<TRequest, TResponse>(TRequest command) where TRequest : Command<TResponse>;
        Task RaiseEvent<T>(T @event) where T : Event;

    }
}
