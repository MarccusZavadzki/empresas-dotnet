using Imdb.CrossCutting.IoC;
using Imdb.Domain.Utils;
using Imdb.WebApi.Configuration;
using MediatR;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;

namespace Imdb.WebApi {

    public class Startup {
        public IConfiguration Configuration { get; }

        public Startup(IConfiguration configuration) {
            Configuration = configuration;
        }

        public void ConfigureServices(IServiceCollection services) {

            Settings.SecretKey = Configuration["SecretKey"];
            
            services.AddCorsConfiguration();
            services.AddControllers();
            services.AddAuthenticationConfiguration();
            services.AddSwaggerConfiguration();

            services.AddMediatR(typeof(Startup));

            RegisterServices(services, Configuration);
        }

        public void Configure(IApplicationBuilder app, IWebHostEnvironment env) {

            if(env.IsDevelopment())
                app.UseDeveloperExceptionPage();

            app.UseHttpsRedirection();
            app.UseRouting();

            app.UseCorsSetup();
            app.UseAuthenticationSetup();

            app.UseEndpoints(endpoints => {
                endpoints.MapControllers();
            });

            app.UseSwaggerSetup();
        }

        private static void RegisterServices(IServiceCollection services, IConfiguration configuration) {
            NativeInjectorBootStrapper.RegisterServices(services, configuration);
        }
    }
}
