﻿using Microsoft.AspNetCore.Builder;
using Microsoft.Extensions.DependencyInjection;
using System;

namespace Imdb.WebApi.Configuration {

    public static class CorsConfig {

        public static void AddCorsConfiguration(this IServiceCollection services) {

            if(services == null)
                throw new ArgumentNullException(nameof(services));

            services.AddCors();

        }

        public static void UseCorsSetup(this IApplicationBuilder application) {

            if(application == null)
                throw new ArgumentNullException(nameof(application));

            application.UseCors(x => x
                .AllowAnyOrigin()
                .AllowAnyMethod()
                .AllowAnyHeader());
        }
    }
}
