﻿using Imdb.Domain.Utils;
using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.AspNetCore.Builder;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.IdentityModel.Tokens;
using System;
using System.Text;

namespace Imdb.WebApi.Configuration {

    public static class AuthenticationConfig {

        public static void AddAuthenticationConfiguration(this IServiceCollection services) {

            if(services == null)
                throw new ArgumentNullException(nameof(services));

            var key = Encoding.ASCII.GetBytes(Settings.SecretKey);
            services.AddAuthentication(x => {
                x.DefaultAuthenticateScheme = JwtBearerDefaults.AuthenticationScheme;
                x.DefaultChallengeScheme = JwtBearerDefaults.AuthenticationScheme;
            })
            .AddJwtBearer(x => {
                x.RequireHttpsMetadata = false;
                x.SaveToken = true;
                x.TokenValidationParameters = new TokenValidationParameters {
                    ValidateIssuerSigningKey = true,
                    IssuerSigningKey = new SymmetricSecurityKey(key),
                    ValidateIssuer = false,
                    ValidateAudience = false
                };
            });

        }

        public static void UseAuthenticationSetup(this IApplicationBuilder application) {

            if(application == null)
                throw new ArgumentNullException(nameof(application));

            application.UseAuthentication();
            application.UseAuthorization();

        }

    }
}
