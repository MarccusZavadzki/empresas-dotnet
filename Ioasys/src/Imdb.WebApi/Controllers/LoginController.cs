﻿using Imdb.ApplicationService.ViewModels.Request;
using Imdb.AuthenticationService;
using Imdb.Domain.Core.Bus;
using Imdb.Domain.Core.Models;
using Imdb.Domain.Interfaces.Repositories;
using Imdb.WebApi.Controllers.Base;
using MediatR;
using Microsoft.AspNetCore.Mvc;
using System.Threading.Tasks;

namespace Imdb.WebApi.Controllers {

    [ApiController]
    [Route("[controller]")]
    public class LoginController : BaseController {

        private readonly IUserRepository _userRepository;

        public LoginController(IMediatorHandler mediatorHandler, INotificationHandler<DomainNotification> notifications, IUserRepository userRepository) : base(notifications, mediatorHandler) {
            _userRepository = userRepository;
        }

        [HttpPost]
        public async Task<ActionResult<dynamic>> Authenticate([FromBody] LoginViewModel loginViewModel) {
            
            var user = await _userRepository.GetUserToLogin(loginViewModel.Email, loginViewModel.Password);

            if(user == null)
                return NotFound(new { message = "Email ou senha inválidos" });

            var token = TokenService.GenerateToken(user);
            user.Password = string.Empty;

            return new { user, token };
        }

    }
}
