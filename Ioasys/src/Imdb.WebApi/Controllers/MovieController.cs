﻿using Imdb.ApplicationService.Interfaces;
using Imdb.ApplicationService.ViewModels.Request.Movie;
using Imdb.ApplicationService.ViewModels.Response;
using Imdb.Domain.Core.Bus;
using Imdb.Domain.Core.Models;
using Imdb.WebApi.Controllers.Base;
using MediatR;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System.Collections.Generic;
using System.Security.Claims;
using System.Threading.Tasks;

namespace Imdb.WebApi.Controllers {

    [ApiController]
    [Route("[controller]")]
    public class MovieController : BaseController {

        private readonly IMovieApplicationService _movieApplicationService;

        public MovieController(
            IMediatorHandler mediatorHandler,
            INotificationHandler<DomainNotification> notifications,
            IMovieApplicationService movieApplicationService
        ) : base(notifications, mediatorHandler) {
            _movieApplicationService = movieApplicationService;
        }

        /// <summary>
        ///     Get Movie By id
        /// </summary>
        /// <param name="id"></param>
        /// <returns>Movie searched</returns>
        /// <response code="200">Resume was found</response>
        /// <response code="400">Error in process/response>
        /// <response code="404">Data not found/response>
        /// <response code="500">Returned case internal error</response> 
        [HttpGet]
        [ProducesResponseType(typeof(MovieResponse), StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        [ProducesResponseType(StatusCodes.Status500InternalServerError)]
        public async Task<ActionResult<MovieResponse>> GetById(int id) {

            var result = await _movieApplicationService.Get(id);

            return Response(result);
        }

        /// <summary>
        ///     List movies with filter
        /// </summary>
        /// <param name="filter"></param>
        /// <param name="page"></param>
        /// <returns>Movies filtereds</returns>
        /// <response code="200">Resume was found</response>
        /// <response code="400">Error in process/response>
        /// <response code="404">Data not found/response>
        /// <response code="500">Returned case internal error</response> 
        [HttpGet]
        [Route("List/{page}")]
        [ProducesResponseType(typeof(List<MovieResponse>), StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        [ProducesResponseType(StatusCodes.Status500InternalServerError)]
        public async Task<ActionResult<List<MovieResponse>>> List([FromBody] FilterMoviesViewModel filter, int page = 0) {

            var result = await _movieApplicationService.List(filter, page);

            return Response(result);
        }

        /// <summary>
        ///     Create a movie
        /// </summary>
        /// <param name="viewModel"></param>
        /// <returns>Movie created</returns>
        /// <response code="200">Resume was found</response>
        /// <response code="400">Error in process/response>
        /// <response code="500">Returned case internal error</response> 
        [HttpPost]
        [Authorize(Roles = "ADM")]
        [ProducesResponseType(typeof(MovieResponse), StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        [ProducesResponseType(StatusCodes.Status500InternalServerError)]
        public async Task<ActionResult<MovieResponse>> Create([FromBody] CreateMovieViewModel viewModel) {

            var result = await _movieApplicationService.Create(viewModel);

            return Response(result);
        }

        /// <summary>
        ///     Rate a movie
        /// </summary>
        /// <param name="idMovie"></param>
        /// <param name="note"></param>
        /// <returns>Movie rated</returns>
        /// <response code="200">Resume was found</response>
        /// <response code="400">Error in process/response>
        /// <response code="500">Returned case internal error</response> 
        [HttpPost]
        [Route("Vote/{idMovie}/{note}")]
        [Authorize(Roles = "ADM,Normal")]
        [ProducesResponseType(typeof(MovieResponse), StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        [ProducesResponseType(StatusCodes.Status500InternalServerError)]
        public async Task<ActionResult<MovieResponse>> Vote(int idMovie, int note) {

            if(note < 0 || note > 4) {
                return BadRequest($"A nota ({note}) não é valida, insira um valor de 1 á 4");
            }

            var userId = User.FindFirstValue(ClaimTypes.NameIdentifier);

            if(userId != null && int.TryParse(userId, out int idUser)) {
                var result = await _movieApplicationService.Vote(idUser, idMovie, note);
                return Response(result);
            }

            return Forbid("Erro de autenticacao");
        }
    }
}
