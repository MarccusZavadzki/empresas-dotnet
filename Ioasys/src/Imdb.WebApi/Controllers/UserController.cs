﻿using Imdb.ApplicationService.Interfaces;
using Imdb.ApplicationService.ViewModels.Request;
using Imdb.ApplicationService.ViewModels.Response;
using Imdb.Domain.Core.Bus;
using Imdb.Domain.Core.Models;
using Imdb.WebApi.Controllers.Base;
using MediatR;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Imdb.WebApi.Controllers {

    [ApiController]
    [Route("[controller]")]
    public class UserController : BaseController {

        private readonly IUserApplicationService _userApplicationService;

        public UserController(
            IMediatorHandler mediatorHandler,
            INotificationHandler<DomainNotification> notifications,
            IUserApplicationService userApplicationService
        ) : base(notifications, mediatorHandler) {
            _userApplicationService = userApplicationService;
        }

        /// <summary>
        ///     Get Ative users 
        /// </summary>
        /// <param name="page"></param>
        /// <returns>List of users ative</returns>
        /// <response code="200">Resume was found</response>
        /// <response code="400">Error in process/response>
        /// <response code="404">Data not found/response>
        /// <response code="500">Returned case internal error</response> 
        [HttpGet]
        [Route("List/{page}")]
        [Authorize(Roles = "ADM")]
        [ProducesResponseType(typeof(List<MovieResponse>), StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        [ProducesResponseType(StatusCodes.Status500InternalServerError)]
        public async Task<ActionResult<List<MovieResponse>>> GetAtiveUsers(int page = 0) {

            var result = await _userApplicationService.GetAtiveUsers(page);

            return Response(result);
        }

        /// <summary>
        ///     Create a User
        /// </summary>
        /// <param name="viewModel"></param>
        /// <returns>User created</returns>
        /// <response code="200">Resume was found</response>
        /// <response code="400">Error in process/response>
        /// <response code="500">Returned case internal error</response> 
        [HttpPost]
        [Authorize(Roles = "ADM")]
        [ProducesResponseType(typeof(MovieResponse), StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        [ProducesResponseType(StatusCodes.Status500InternalServerError)]
        public async Task<ActionResult<MovieResponse>> Create([FromBody] CreateUserViewModel viewModel) {

            var result = await _userApplicationService.Create(viewModel);

            return Response(result);
        }

        /// <summary>
        ///     Update a User
        /// </summary>
        /// <param name="viewModel"></param>
        /// <returns>User updated</returns>
        /// <response code="200">Resume was found</response>
        /// <response code="400">Error in process/response>
        /// <response code="500">Returned case internal error</response> 
        [HttpPut]
        [Authorize(Roles = "ADM")]
        [ProducesResponseType(typeof(MovieResponse), StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        [ProducesResponseType(StatusCodes.Status500InternalServerError)]
        public async Task<ActionResult<MovieResponse>> Update([FromBody] UpdateUserViewModel viewModel) {

            var result = await _userApplicationService.Update(viewModel);

            return Response(result);
        }

        /// <summary>
        ///     Delete a User (Inative)
        /// </summary>
        /// <param name="id"></param>
        /// <response code="200">Resume was found</response>
        /// <response code="400">Error in process/response>
        /// <response code="500">Returned case internal error</response> 
        [HttpPut]
        [Route("Inative/{id}")]
        [Authorize(Roles = "ADM")]
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        [ProducesResponseType(StatusCodes.Status500InternalServerError)]
        public async Task<IActionResult> Inative(int id) {

            await _userApplicationService.Delete(id);

            return Response();
        }

    }
}
