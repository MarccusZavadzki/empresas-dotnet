﻿using AutoMapper;
using Imdb.ApplicationService.AutoMapper;
using Imdb.ApplicationService.Interfaces;
using Imdb.ApplicationService.Services;
using Imdb.CrossCutting.Bus;
using Imdb.Data.SqlServer.Context;
using Imdb.Data.SqlServer.Repositories;
using Imdb.Domain.CommandHandlers;
using Imdb.Domain.Commands.Movie;
using Imdb.Domain.Commands.User;
using Imdb.Domain.Core.Bus;
using Imdb.Domain.Core.Models;
using Imdb.Domain.Core.Notifications;
using Imdb.Domain.Interfaces.Repositories;
using Imdb.Domain.Models.Entities;
using MediatR;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc.Infrastructure;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;

namespace Imdb.CrossCutting.IoC {

    public static class NativeInjectorBootStrapper {

        public static void RegisterServices(this IServiceCollection services, IConfiguration configuration) {

            // ASP.NET HttpContext dependency
            services.AddSingleton<IHttpContextAccessor, HttpContextAccessor>();
            services.AddSingleton<IActionContextAccessor, ActionContextAccessor>();

            var mapperConfig = new MapperConfiguration(mc => {
                mc.AddProfile(new AutomapperConfig());
            });
            mapperConfig.AssertConfigurationIsValid();
            IMapper mapper = mapperConfig.CreateMapper();
            services.AddSingleton(mapper);

            // ApplicationService
            services.AddScoped<IUserApplicationService, UserApplicationService>();
            services.AddScoped<IMovieApplicationService, MovieApplicationService>();

            // Domain Bus (Mediator)
            services.AddScoped<IMediatorHandler, InMemoryBus>();
            // Domain - Notification
            services.AddScoped<INotificationHandler<DomainNotification>, DomainNotificationHandler>();

            // Domain - Commands
            //// User
            services.AddScoped<IRequestHandler<CreateUserCommand, User>, UserCommandHandler>();
            services.AddScoped<IRequestHandler<UpdateUserCommand, User>, UserCommandHandler>();
            services.AddScoped<IRequestHandler<DeleteUserCommand, Unit>, UserCommandHandler>();

            //// Movie
            services.AddScoped<IRequestHandler<CreateMovieCommand, Movie>, MovieCommandHandler>();
            services.AddScoped<IRequestHandler<RateMovieCommand, Movie>, MovieCommandHandler>();

            //// Entity Framework Core
            var connectionString = configuration.GetConnectionString("DefaultConnection");

            services.AddDbContext<ApplicationDbContext>(options => {
                options.UseSqlServer(connectionString);
            });

            // Repositories - SqlServer
            services.AddScoped<IUserRepository, UserRepository>();
            services.AddScoped<IMovieRepository, MovieRepository>();
            services.AddScoped<IMovieActorsRepository, MovieActorsRepository>();
            services.AddScoped<IMovieRatingRepository, MovieRatingRepository>();
        }

    }
}
